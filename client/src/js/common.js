define(function (require,exports,modules) {
   let $=require('jquery');
   require('lazyload');


   //滑动块方法
    let slider=function () {
        $('#slider').mousedown(function () {

            $("#sliderBox").mousemove(function (e) {

                var mx=e.pageX - $('#sliderBox').offset().left;
                if (mx<=0){
                    mx = 0;
                }
                if (mx>=$("#sliderBox").width()-$("#slider").width()){
                    mx = $("#sliderBox").width()-$("#slider").width();
                }


                $('#slider').css({"left":mx , "top":0});

            }).mouseup(
                function () {
                    $("#sliderBox").off('mousemove');
                });
            //为了便于判断,让这个函数返回一个bool值,布尔值表示滑动到了最右边


        });
        return $("#slider").position().left>=$("#sliderBox").width()-$("#slider").width();

    };
//图片缓存方法
    let lazyimg=function () {
        //图片懒加载
        $('img.lazy').lazyload({
            placeholder:'./img/grey.gif',
            effect:'fadeIn'
        }).removeClass('lazy')//为了防止动态添加图片导致之前的图片重新懒加载,在这里移除lazy属性
    }
//topnav加载的方法
    let topInit=function(){
        let sessioninfo = JSON.parse(sessionStorage.getItem('userInfo'));

        if (sessioninfo) {
            //console.log(sessioninfo["u_tel"]);
            let user = sessioninfo["u_tel"];
            user = user.substring(0, 3) + '****' + user.substring(7, 11);

            //登录后改变a链接的地址
            //                $('#username').attr('href','./login.html')
            $('#username').html(user);

            $('#mymsg').show();
            $('#mybeibei').show();
            $('#myorder').show();
            $('#exit').show();

            $('#exit').click(function () {//退出登录按钮
                sessionStorage.removeItem('userInfo');
                $('#username').html('请您登录');
                $('#username').attr('href', './login.html');
                $('#mymsg').hide();
                $('#mybeibei').hide();
                $('#myorder').hide();
                $(this).hide();

            })
        } else {
            $('#username').html('请您登录');
            $('#username').attr('href', './login.html')
            $('#mymsg').hide();
            $('#mybeibei').hide();
            $('#myorder').hide();
            $('#exit').hide();

        }
        //nav栏点击变化颜色
        $('.nav>.wrap>ul').children('li').click(function () {
            $(this).addClass('current').siblings().removeClass('current');
        })
    }

    //首页购物车分类栏显示和隐藏
    let clarify = function () {
        //在售分类展开


        $('#clarify').mouseenter(function () {
            $('.collection').show().mouseenter(function () {
                $(this).show();

            }).mouseleave(function () {
                $(this).hide();
            });
        })
        $('.search').mouseleave(function () {
            $('.collection').hide();
        })


        $('.clc li').mouseenter(function () {
            $(this).addClass('current').siblings().removeClass('current');
            $('.clcdisplay ul').eq($(this).index()).show().siblings().hide();
        })


    }
    //倒计时
    let countdown=function(countdownDate,callback){//这里countdownDate传入未来的日期,格式为"2018/12/28"
        let o={}
        let currentTime={};
        //从服务器读取时间
        $.ajax({

            url:"http://localhost/beibeiserver/server/getTime.php",

            success:function(data){
                let futureTime=new Date(countdownDate).getTime();
                // alert(data.date);
               o.timer = setInterval(function () {
                    currentTime=new Date().getTime();
                   o.plus=futureTime-currentTime;

                    o.day = parseInt(o.plus / 1000 / 60 / 60/24 );
                    o.day=o.day<10?0+''+o.day:o.day;
                    o.hour = parseInt(o.plus / 1000 / 60 / 60)-o.day*24;
                   o.hour=o.hour<10?0+''+o.hour:o.hour;

                       o.minute = parseInt(o.plus/1000/60)-parseInt(o.plus / 1000 / 60 / 60) * 60;
                   o.minute=o.minute<10?0+''+o.minute:o.minute;

                   o.second = o.plus/1000%60;
                        //parseInt(o.plus/1000)%60;
                   o.second=o.second<10?0+''+o.second.toFixed(1):o.second.toFixed(1);

                   o.ms=o.plus%1000;
                        if (o.plus<=0){
                            clearInterval(o.timer);
                        }
                    callback(o);//通过回调函数返回
                },100)




            }

        })



    }


    modules.exports={
        slider:slider,
        lazyimg:lazyimg,
        topInit:topInit,
        clarify:clarify,
        countdown:countdown
    }
})