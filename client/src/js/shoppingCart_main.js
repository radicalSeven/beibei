require.config({
    baseUrl:"./js/lib",
    paths:{
        "jquery":"jquery-1.11.1.min",
        "cookie":"jquery.cookie",
        "lazyload":"jquery.lazyload.min",
        "shoppingCart":['../shoppingCart'],
        "common":["../common"]

    },
    shim:{
        lazyload:{
            deps:['jquery']
        }
    }

})

require(['shoppingCart','jquery','common'],function (shoppingCart,$,common) {
    common.topInit();
    shoppingCart.cart();
})