define(function (require, exports, modules) {
    let $ = require('jquery');
    let cart = function () {
        //判断是否登录
        // console.log(1250+1423+2100+1226+1250+1128+1226+1325+1030-500-800-1749+600+250);

        let sessioninfo = JSON.parse(sessionStorage.getItem('userInfo'));
        if (sessioninfo) {
            let c_buyerTel = sessioninfo['u_tel'];
            console.log(c_buyerTel);
            //向服务器询问该账户的购物车列表
            $.ajax({
                type: "POST",
                url: "http://localhost/beibeiserver/server/getCartsByUser.php",
                data: {
                    c_buyerTel: c_buyerTel
                },
                success: function (res) {

                    if (res.length == 0) {//
                        //res没有数据时返回一个空数组
                        $(".myCartList").empty();


                    } else {
                        //拿到数据进行遍历
                        //购物车商品按照商品卖家分类
                        function dataClarify(res) {//处理数组分类函数
                            let data = [];

                            for (var i = 0; i < res.length; i++) {
                                if (!data[res[i].c_salername]) {
                                    var arr = [];
                                    arr.push(res[i]);
                                    data[res[i].c_salername] = arr;
                                } else {
                                    data[res[i].c_salername].push(res[i])
                                }
                            }

                            return data;

                        }

                        let data = dataClarify(res);
                        let str = ``//用来拼接字符串
                        let total = 0;//所有商品总价
                        let count = 0;//共有多少商品
                        let preferentialPrice = 0;//总优惠价格
                        $("#tb").empty();//有返回则清空一下tbody中的数据
                        for (var key in data) {

                            let sum = 0;//统计同一商家商品的总价

                            str += `<tr class="group">
                <td colspan="7" id="c_salername"><input type="checkbox" />${key}</td>
              </tr>`
                            data[key].forEach(function (ele) {
                                str += `<tr>
                <td class="td1"><input type="checkbox" /></td>
                <td class="td2">
                  <a href="productdetail.html?p_id=${ele.product_id}">
                    <img src="img/displayRightToy01.jpg" alt="" /><span
                     id="c_productname" >${ele.c_productname}</span
                    ></a
                  >
                </td>
                <td class="td3" id="c_info">${ele.c_info}</td>
                <td class="td4"><span id="c_currentprice">${ele.c_currentprice}</span><em id="c_oldprice">${ele.c_oldprice}</em></td>
                <td class="td5">
                  <div>
                    
                    <em><i></i></em> <input type="text" readonly value=${ele.c_purchasenum} id="c_purchasenum"/>
                    <em><i></i></em>
                  </div>
                </td>
                <td class="td6">${(ele.c_purchasenum * ele.c_currentprice).toFixed(2)}</td>
                <td class="td7"><a href="#">删除</a></td>
              </tr>
               `;
                                sum += ele.c_purchasenum * ele.c_currentprice;
                                preferentialPrice += parseFloat(ele.c_oldprice);
                                count += parseInt(ele.c_purchasenum);
                            });
                            total += Number(sum.toFixed(2));
                            str += `<tr class="sum">
                <td colspan="5">小计:</td>
                <td colspan="2">${sum.toFixed(2)}</td>
              </tr>`

                        }
                        $("#tb").html($(str))
                        $('#totalNum').text(count);
                        $('#totalPrice').text('¥' + total.toFixed(2));
                        $('#preferentialPrice').text('¥' + preferentialPrice.toFixed(2))
                    }


                    //页面加载完成后进行相关操作
                    //表单shoppingCartContent高度大于800px时,让底部的shoppingCartBottomWrap fixed一下
                    function bottomScroll() {
                        if ($(".shoppingCart").height()>=800){
                            let left=$("table").offset().left;
                            $(".shoppingCartBottomWrap").addClass('current').css('left',left);
                        }

                        console.log($(".shoppingCartBottom").offset().top,$(document).scrollTop(),$(window).height());

                        if ($(".shoppingCartBottom").offset().top-$(document).scrollTop()+60<=$(window).height()){
                            $(".shoppingCartBottomWrap").removeClass('current');
                        }
                    }


                    //添加滚轮事件
                    $(window).scroll(function () {
                        bottomScroll();
                    })
                    bottomScroll();//防止页面刷新时导致shoppingCartBottomWrap没响应



                }
            })
        }

    };


    exports.cart = cart;
})