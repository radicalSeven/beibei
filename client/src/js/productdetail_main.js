require.config({
    baseUrl:"./js/lib",
    paths:{
        "jquery":"jquery-1.11.1.min",
        "cookie":"jquery.cookie",
        "lazyload":"jquery.lazyload.min",
        "productdetail":['../productdetail'],
        "index":['../index'],
        "lazyimg":['../lazyload'],
        'common':['../common']


    },
    shim:{
        lazyload:{
            deps:['jquery']
        }
    }

})

require(['jquery','productdetail','index','lazyimg','common'],function ($,productdetail,index,lazyimg,common) {
  common.lazyimg();
    common.topInit();
    common.clarify();
    productdetail.init();
    productdetail.productdetail();
})