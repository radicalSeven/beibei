require.config({
    baseUrl:"./js/lib",
    paths:{
        "jquery":"jquery-1.11.1.min",
        "jquery.validate":"jquery.validate",
        "additional-methods":"additional-methods",
        "register":['../register'],
        "idcode":["jquery.idcode"],
        "slider":['../slider']

    },
    shim:{
        idcode:{
            deps:["jquery"]//给idcode添加依赖
        }
    }
});

//使用模块
require(['register','jquery','slider'],function (register, $,slider) {
    slider.slider();
    register.checkCode();
    register.reg();
})