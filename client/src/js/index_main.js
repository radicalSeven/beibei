require.config({
    baseUrl:"./js/lib",
    paths:{
        "jquery":"jquery-1.11.1.min",
        "cookie":"jquery.cookie",
        "lazyload":"jquery.lazyload.min",
        "index":['../index'],
        "common":["../common"]

    },
    shim:{
        lazyload:{
            deps:['jquery']
        }
    }

})

require(['index','jquery','common'],function (index,$,common) {
common.topInit();
common.clarify();
common.lazyimg();
index.init();
index.index();
})