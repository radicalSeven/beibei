define(function (require,exports,modules) {
    require('lazyload');
    let $=require('jquery');
    let lazyimg=function () {
        //图片懒加载
        $('img.lazy').lazyload({
            placeholder:'./img/grey.gif',
            effect:'fadeIn'
        })
    }
    exports.lazyimg=lazyimg;
})