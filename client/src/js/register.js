
define(function (require,exports,modules) {
    let $=require("jquery");
    require("jquery.validate");
    require("additional-methods");
    require('idcode');
    let slider=require('slider')


    $.validator.addMethod('checkTel',function (val,ele) {
        return /^1\d{10}$/.test(val);
    },'手机号码不合法');

    let reg=function () {
        $('form').validate({
            rules:{
                uTel:{
                    required:true,
                    checkTel:true,
                    remote:{
                        url:"http://localhost/beibeiserver/server/checkName.php",
                        type:'post',
                        dataType:'json',
                        data:{
                            uTel:function () {
                                return $('.uTel').val();

                        }
                    }
                }
                },
                uPwd:{
                    required:true,
                    rangelength:[6,16]
                }
            },
            messages:{
                uTel:{
                    required:'手机号不能为空',
                    remote:'该手机号已注册'
                },
                uPwd:{
                    required:'密码不能为空',
                    rangelength:'密码为{0}-{1}位'
                }
            },

            submitHandler:function () {
                let checkCodeStatus=$.idcode.validateCode();
                let sliderStatus=slider.slider();//判断滑块是不是在最右边
                if (checkCodeStatus&&sliderStatus){
                    $.ajax({
                        type:"POST",
                        url:'http://localhost/beibeiserver/server/register.php',
                        data:{
                            uTel:$('.uTel').val(),
                            uPwd:$('.uPwd').val()
                        },
                        success:function (res) {
                            if (res.status==1){
                                alert('恭喜注册成功');
                                //成功后清空手机和验证码
                                $('.uTel').val('');
                                $('.uPwd').val('');
                                $(location).attr('href', './login.html');

                            }
                        }
                    })
                }else {
                    alert('验证码不正确')
                }

                return false;
            }
        })
    }
    let checkCode=function(){
        $.idcode.setCode();
    }

    //暴露方法
    exports.checkCode=checkCode;
    exports.reg=reg;

})