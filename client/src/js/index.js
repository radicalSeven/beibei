
define(function (require, exports, modules) {


    let $ = jQuery = require('jquery');
   let common= require('common');


   let init=function () {

       let sessioninfo = JSON.parse(sessionStorage.getItem('userInfo'));


       if (sessioninfo) {
           let c_buyerTel = sessioninfo['u_tel'];
           console.log(c_buyerTel);
           //向服务器询问该账户的购物车列表
           $.ajax({
               type: "POST",
               url: "http://localhost/beibeiserver/server/getCartsByUser.php",
               data: {
                   c_buyerTel: c_buyerTel
               },
               success: function (res) {
                   console.log(res);
                   if (res.length == 0) {//
                       //res没有数据时返回一个空数组
                      $('#myCartEmpty').show();
                       $('#myCartList').hide();
                       $('#toPay strong').hide();
                       $('#toPay a').show().text('去购物').attr('href','index.html')

                   } else {
                       //拿到数据进行遍历
                       //购物车商品按照商品卖家分类
                       $('#myCartEmpty').hide();
                       $('#myCartList').show();
                       $('#toPay strong').show();
                       $('#toPay a').show().text('去购物车结算').attr('href','shoppingCart.html')

                       let str = ``//用来拼接字符串
                       let total = 0;//所有商品总价
                       $("#myCartList").empty();//有返回则清空一下tbody中的数据
                       for (var key in res) {

                           let sum = 0;//统计同一商家商品的总价
                            let ele=res[key];
                           str += `
                           <li>
                           <a href="shoppingCart.html">
                            <img src="img/productdetail/zoom/small60*60_01.jpg" alt="" id="c_img">
                            <p id="productInfo"><strong id="c_productname">${ele.c_productname.substring(0,20)}</strong><i id="c_info">${ele.c_info}</i></p>
                            <p id="productPrice"><strong id="c_currentprice">${Number(ele.c_currentprice).toFixed(2)}</strong><i id="c_purchasenum">X${ele.c_purchasenum}</i></p>
                        </a>
                        </li>
               `;

                           total += Number(ele.c_currentprice);


                       }

                      $("#myCartList").html($(str))
                       $('#sumPrice').text('¥' +total.toFixed(2));
                   }




               }
           })
       }

   }



    let index = function () {
//内容区右侧hover
        $('.displayRightToplist').children('li').hover(function () {
            $(this).addClass('current').siblings().removeClass('current')
            $('.displayRightListWrap').children('ul').eq($(this).index()).show().siblings().hide();
        })

        //    loadingmore
        $("#loadingmore").click(function () {
                let str=``;
                for(let i=0;i<21;i++){

                    let randon=parseInt(Math.random()*6+1);

                    //数据需要从数据库加载
                    str+= ` <div class="dpl-detail">
                        <a href="productdetail.html?p_id=${randon}">
                            <img data-original="img/content0${randon}.jpg" alt="" class="lazy">
                            <p>男童棉裤加绒保暖休闲帅气童裤</p>
                            <div class=" price-info">
                                <span>¥37.90</span>
                                <i>¥108.9</i>
                                <em>3.5折</em>
                            </div>
                        </a>
                    </div>`

                }
                $(".displayLeftWrap").append($(str));
                //添加完成后懒加载一下
            common.lazyimg();

            }

        )
        
        //设置顶部悬挂导航栏
        $(window).scroll(function () {
            topStatus();
        })
        topStatus();//防止刷新时悬挂导航栏不显示
        function topStatus() {
            let topGap=$(".top").height()+$(".logowrap").height()+$(".nav").height();
            if ($(document).scrollTop()>topGap){
                $(".suspendnav").show();
            }else {
                $(".suspendnav").hide();

            }

        }

    }



    exports.index = index;
   exports.init=init;
})
