define(function (require, exports, modules) {
    let $ = require('jquery');
    require('lazyload');
    let common = require('common');
    //init开始*********************************
    let currenturl=window.location.href;
    let p_id= currenturl.split('=')[1];
    let init=function () {
        //根据p_id读取商品
        //获取p_id

        $.ajax({
            url:'http://localhost/beibeiserver/server/getProductById.php',
            type:'post',
            data:{
                p_id:p_id
            },
            success:function (res) {
                console.log(res);


                //p_bigimg: null
                // p_countdown: "2018/12/30"
                // p_currentprice: "19.90"
                // p_id: 2
                // p_info: "简约时尚儿童雪地棉靴，鞋底轻便，可爱时尚，加绒加厚，温馨提示；靴子不是标准码，按脚长+0.5cm对应内长购买，大家量好购买哟，不要到时说鞋子不标准.内里加绒毛，有轻微掉毛属于正常范围.手工测量内长有0.3cm左右误差，按脚长+0.5cm对应内长购买"
                // p_middleimg: null
                // p_name: " 加绒加厚 舒适护脚 】儿童雪地靴冬季棉靴保暖短靴平底防滑男女童棉鞋儿童靴子学"
                // p_oldprice: "198.00"
                // p_size: "["25码","25码","25码","25码","25码","25码"]"
                // p_smallimg: null
                // p_stock: "300"
                var res=res[0];
                console.log(res.p_name);
                $('.productname').text(res.p_name);
                $('#newPrice').text(res.p_currentprice);
                $('.oldPrice').text(res.p_oldprice);
                $('#discount').text((10*res.p_currentprice/res.p_oldprice).toFixed(1)+'折');
                $('#saved').text((res.p_oldprice-res.p_currentprice).toFixed(2));
                //buyOnePrice小数点部分要处理一下
                let priceDecimal='.'+(res.p_currentprice-parseInt(res.p_currentprice)).toFixed(2).split('.')[1]
                $('#buyOnePrice').html(`¥<em>${parseInt(res.p_currentprice)}</em><b>${priceDecimal}</b>`);

                //res.p_countdown
                console.log(res.p_countdown);
                //倒计时开始,设置的时间从数据库获取---------------------------
                common.countdown(res.p_countdown, function (date) {

                    $("#day").text(date.day);
                    $("#hours").text(date.hour);
                    $("#minutes").text(date.minute);
                    $("#seconds").text(date.second);

                })
                //倒计时结束,设置的时间从数据库获取---------------------------


            }
        });

        $('#exit').click(function () {//退出登录按钮
            sessionStorage.removeItem('userInfo');
            $('#username').html('请您登录');
            $('#username').attr('href', './login.html');
            $('#mymsg').hide();
            $('#mybeibei').hide();
            $('#myorder').hide();
            $(this).hide();

        })
        //避免登录后退出,但是选择了商品tobuy点击依然可用的问题
        $('#tobuy').click(function () {
            if (!sessionStorage.getItem('userInfo')){
                $(this).attr('href','javascript:;;');
                if (confirm('您还未登录,是否选择登录')) {
                    $(location).attr('href', './login.html')
return;
                }
            }
        })

    }
    
    
    
    
    //init结束*********************************


    //productdetail开始*********************************
    let productdetail = function () {

// 放大镜开始
        $('.orig').mouseenter(function () {
            $('.bigArea').show();
            $('.smallArea').show();
        })

        $('.orig').mousemove(function (e) {
            var bigImg = $('.bigArea img')
            //计算蒙罩的大小
            var mWidth = ($(this).width() / bigImg.width()) * $('.bigArea').width() - 1;
            var mHeight = ($(this).height() / bigImg.height()) * $('.bigArea').height() - 1;
//设置蒙罩的大小
            $(".smallArea").css({height: mHeight, width: mWidth})
            var scale = bigImg.width() / $(this).width() * -1;
//鼠标点距离图片区域的边界

            var disX = e.pageX - $(this).offset().left - $(".smallArea").width() / 2;
            var disY = e.pageY - $(this).offset().top - $(".smallArea").height() / 2;
            if (disX <= 0) {
                disX = 0;
            }
            if (disY <= 0) {
                disY = 0;
            }
            var gapX = $(this).width() - $('.smallArea').width();
            var gapY = $(this).height() - $('.smallArea').height();

            if (disX >= gapX) {
                disX = gapX;
            }
            if (disY >= gapY) {
                disY = gapY;
            }

            $(".smallArea").css({left: disX, top: disY});

            var x = $(".smallArea").position().left;
            var y = $(".smallArea").position().top;
            $(bigImg).css({left: x * scale, top: y * scale})
            //获取小区域的相对坐标点
//    var x=$(".smallArea").position().left;
//    var y=$(".smallArea").position().top;
        })
        $('.orig').mouseleave(function () {
            $('.bigArea').hide();
            $('.smallArea').hide();

        })
//放大镜结束
        //点击图片切换图片开始
        $('.bottomlist').children().click(function () {
            $(this).addClass('current').siblings().removeClass('current');
            $('.orig img').attr('src', 'img/productdetail/zoom/middle460*460_0' + ($(this).index() + 1) + '.jpg')
            $('.bigArea img').attr('src', 'img/productdetail/zoom/big800*800_0' + ($(this).index() + 1) + '.jpg')

        })
        $(document).on('click','.colorList>li',function () {
            $('.orig img').attr('src', 'img/productdetail/zoom/middle460*460_0' + ($(this).index() + 1) + '.jpg')
            $('.bigArea img').attr('src', 'img/productdetail/zoom/big800*800_0' + ($(this).index() + 1) + '.jpg')
            $('.bottomlist li').eq($(this).index() ).addClass('current').siblings().removeClass('current');
            //当索引值大于等于4时
            if($(this).index()>=4){
                $('.bottomlist').css({left:-84*4});
            }else {
                $('.bottomlist').css({left:0});

            }

        })



        //添加至购物车开始----------------------------------------
        //点击购买时将商品放入购物车
        //准备所需参数,这个应该从数据库直接拿
        let c_productname = $('.productname').text().trim(),
            c_oldprice = $('.oldPrice').text().trim(),
            c_salername = $('#brand').text(),
            c_currentprice = $('#newPrice').text().trim(),
            c_info = null,//这个为颜色和尺寸信息
            //定义一下颜色和尺寸方便判断
            c_color = null,
            c_size = null,
            c_purchasenum = null,
            product_id = p_id,
            sessioninfo = JSON.parse(sessionStorage.getItem('userInfo')),
            c_buyerTel =null;
            if(sessioninfo){

            c_buyerTel = sessioninfo.u_tel;
        }


        //c_productname,c_currentprice,c_oldprice,c_salername,c_info,c_purchasenum,c_buyerTel,product_id
        //添加购物车的时候判断是否有用户登录,没有则调入登录界面

        //颜色和尺寸的获取
        $(document).on('click', '.colorList>li', function () {
            $(this).toggleClass('current').siblings().removeClass('current');

            if ($(this).hasClass('current')) {
                c_color = $(this).text();
            }
        });
        $(document).on('click', '.sizeList>li', function () {
            $(this).toggleClass('current').siblings().removeClass('current');

            if ($(this).hasClass('current')) {
                c_size = $(this).text();
            }
        });

        //加减购买数量
        $(document).on('click', '.mine', function () {
            c_purchasenum -= 1;
            if (c_purchasenum <= 1) {
                c_purchasenum = 1;
            }
            $("#purchasenum").val(c_purchasenum);


        });
        $(document).on('click', '.plus', function () {
            c_purchasenum = parseInt(c_purchasenum) + 1;
            //这里其实要和数据库中的库存进行一下比较,购买数量不能超过库存数量
            $("#purchasenum").val(c_purchasenum);

        })


        //点击购买,向服务器发送数据
        $(document).on('click', '.clickbuyone', function () {
            //console.log(c_size, c_color);
            c_info = c_size + ',' + c_color;
            // console.log(c_productname, c_currentprice, c_oldprice, c_salername, c_info, c_purchasenum, c_buyerTel, product_id);

            if (!sessioninfo) {
                if (confirm('您还未登录,是否选择登录')) {
                    $(location).attr('href', './login.html')

                }
                return;//直接返回
            }
            c_purchasenum = $("#purchasenum").val();
            console.log(c_purchasenum);

            //判断商品的东西是不是都添加了
            if (c_productname && c_currentprice && c_oldprice && c_salername && c_size && c_color && c_purchasenum && c_buyerTel && product_id) {

                $.ajax({
                    url: "http://localhost/beibeiserver/server/addCart.php",
                    type: 'POST',
                    data: {
                        c_productname: c_productname,
                        c_currentprice: c_currentprice,
                        c_oldprice: c_oldprice,
                        c_salername: c_salername,
                        c_info: c_info,
                        c_purchasenum: c_purchasenum,
                        c_buyerTel: c_buyerTel,
                        product_id: product_id
                    },
                    success: function (res) {
                        console.log(res)
                        if (res.status == 1) {
                            $('.buyOne').attr('class','buyOne current');
                            $('#tobuy').show();
                            $('#buyOnePrice').hide();
                            $('.buyOne>i').hide();
                            $(".continueShopping").show();
                        }
                    }
                })
            }


        })

//继续购物
       $(".continueShopping").click(function () {
           $(this).hide();
           $('.buyOne').attr('class','buyOne clickbuyone');
           $('#tobuy').hide();
           $('#buyOnePrice').show();
           $('.buyOne>i').show();
       })
        //添加至购物车结束----------------------------------------



    }
    //productdetail结束*********************************

    exports.productdetail = productdetail;

exports.init=init;
})



